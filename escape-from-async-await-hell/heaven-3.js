// async 関数自体を await すると、順次実行になる

(async () => {
  console.log((new Date().getTime() % 153829260000) / 1000);
  await selectPizza();
  await selectDrink();
  orderItems();
  console.log((new Date().getTime() % 153829260000) / 1000);
})();

async function selectPizza() {
  console.log("selectPizza");
  await getPizzaData();
  choosePizza();
  await addPizzaToCart();
}

async function selectDrink() {
  console.log("selectDrink");
  await getDrinkData();
  chooseDrink();
  await addDrinkToCart();
}

function getPizzaData() {
  return new Promise(resolve => {
    setTimeout(() => {
      console.log("getPizzaData");
      resolve(true);
    }, 1000);
  });
}

function getDrinkData() {
  return new Promise(resolve => {
    setTimeout(() => {
      console.log("getDrinkData");
      resolve(true);
    }, 2000);
  });
}

function choosePizza() {
  console.log("choosePizza");
  return true;
}

function chooseDrink() {
  console.log("chooseDrink");
  return true;
}

function addPizzaToCart() {
  return new Promise(resolve => {
    setTimeout(() => {
      console.log("addPizzaToCart");
      resolve(true);
    }, 1000);
  });
}

function addDrinkToCart() {
  return new Promise(resolve => {
    setTimeout(() => {
      console.log("addDrinkToCart");
      resolve(true);
    }, 2000);
  });
}

function orderItems() {
  console.log("orderItems");
  return true;
}
