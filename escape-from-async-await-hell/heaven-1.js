// async 関数の返り値を await すると、並列実行になる

(async () => {
  console.log((new Date().getTime() % 153829260000) / 1000);
  const pizzaPromise = selectPizza();
  const drinkPromise = selectDrink();
  await pizzaPromise;
  await drinkPromise;
  orderItems(pizzaPromise, drinkPromise);
  console.log((new Date().getTime() % 153829260000) / 1000);
})();

async function selectPizza() {
  console.log("selectPizza");
  await getPizzaData();
  choosePizza();
  await addPizzaToCart();
  return "PIZZA";
}

async function selectDrink() {
  console.log("selectDrink");
  await getDrinkData();
  chooseDrink();
  await addDrinkToCart();
  return "DRINK";
}

function getPizzaData() {
  return new Promise(resolve => {
    setTimeout(() => {
      console.log("getPizzaData");
      resolve(true);
    }, 1000);
  });
}

function getDrinkData() {
  return new Promise(resolve => {
    setTimeout(() => {
      console.log("getDrinkData");
      resolve(true);
    }, 2000);
  });
}

function choosePizza() {
  console.log("choosePizza");
  return true;
}

function chooseDrink() {
  console.log("chooseDrink");
  return true;
}

function addPizzaToCart() {
  return new Promise(resolve => {
    setTimeout(() => {
      console.log("addPizzaToCart");
      resolve(true);
    }, 1000);
  });
}

function addDrinkToCart() {
  return new Promise(resolve => {
    setTimeout(() => {
      console.log("addDrinkToCart");
      resolve(true);
    }, 2000);
  });
}

function orderItems(pizza, drink) {
  console.log("orderItems");
  console.log(pizza, drink);
  return true;
}
