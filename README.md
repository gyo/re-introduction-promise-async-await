# Promise async/await 再入門

ちゃんと理解する。

## 参考

- [9 Promising Promise Tips - DEV Community 👩‍💻👨‍💻](https://dev.to/kepta/promising-promise-tips--c8f)
- [How to escape async/await hell – freeCodeCamp.org](https://medium.freecodecamp.org/avoiding-the-async-await-hell-c77a0fb71c4c)
- [5 Tips and Thoughts on Async / Await Functions](https://start.jcolemorrison.com/5-tips-and-thoughts-on-async-await-functions/)
- [Async/Await Beginners Tips – Chris Burgin – Medium](https://medium.com/@chrisburgin95/async-await-beginner-tips-d4ba66022273)
- [await - JavaScript | MDN](https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Operators/await)
- [async function - JavaScript | MDN](https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Statements/async_function)
