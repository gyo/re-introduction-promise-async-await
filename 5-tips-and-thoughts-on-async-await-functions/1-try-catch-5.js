const functionA = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("a");
      // reject("apple");
    }, 1000);
  });
};

const functionB = async () => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve("b");
    }, 2000);
  });
};

const functionC = async () => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve("c");
    }, 3000);
  });
};

const main = async (paramA, paramB, paramC) => {
  console.log(`${new Date().getTime()}`.slice(-4));

  const responseA = await functionA(paramA);
  const responseB = await functionB(paramB);
  const responseC = await functionC(paramC);

  console.log(responseA, responseB, responseC);
  console.log(`${new Date().getTime()}`.slice(-4));
};

main()
  .then(result => {
    console.log(result);
  })
  .catch(error => {
    console.log(error);
  });
