const functionA = () => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve("a");
    }, 1000);
  });
};

const functionB = async () => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve("b");
    }, 2000);
  });
};

const functionC = async () => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve("c");
    }, 3000);
  });
};

const main = async (paramA, paramB, paramC) => {
  console.log(`${new Date().getTime()}`.slice(-4));
  let responseA;
  let responseB;
  let responseC;

  try {
    responseA = await functionA(paramA);
  } catch (error) {
    throw error;
  }

  try {
    responseB = await functionB(paramB);
  } catch (error) {
    throw error;
  }

  try {
    responseC = await functionC(paramC);
  } catch (error) {
    throw error;
  }

  console.log(responseA, responseB, responseC);
  console.log(`${new Date().getTime()}`.slice(-4));
};

main();
