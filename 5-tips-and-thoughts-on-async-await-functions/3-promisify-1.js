const fs = require("fs");

async function readThings() {
  const file = await fs.readFile(
    "./5-tips-and-thoughts-on-async-await-functions/static/sample.json",
    "utf8"
  );
  return file;
}

(async () => {
  const file = await readThings();
  console.log(file);
})();
