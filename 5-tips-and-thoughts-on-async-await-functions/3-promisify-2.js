const fs = require("fs");
const { promisify } = require("util");
const readFile = promisify(fs.readFile);

async function readThings() {
  const file = await readFile(
    "./5-tips-and-thoughts-on-async-await-functions/static/sample.json",
    "utf8"
  );
  return file;
}

(async () => {
  const file = await readThings();
  console.log(file);
})();
