const fs = require("fs");

exports.fetchResource = id => {
  return new Promise((resolve, reject) => {
    fs.readFile(`./resources/${id}.json`, "utf8", (error, data) => {
      if (error) {
        reject(error);
      } else {
        resolve(JSON.parse(data));
      }
    });
  });
};
