// Do not fear the rejection OR Do not append redundant .catch after every .then

const fs = require("fs");

const fetchData = () => {
  return Promise.resolve(5);
};

const main = () => {
  fetchData()
    .then(result => {
      console.log("then", result);
      throw new Error("Something happened.");
    })
    .catch(result => {
      console.log("then-catch", result.message);
      return Promise.reject(result);
    })
    .then(
      result => {
        console.log("then-catch-then", result);
      },
      result => {
        console.log("then-catch-then-catch", result.message);
      }
    );
};

main();
