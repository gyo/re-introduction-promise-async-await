const promise1 = Promise.resolve(0);
const promise2 = new Promise(resolve => {
  setTimeout(() => {
    resolve(1000);
  }, 1000);
});

Promise.all([promise1, promise2])
  .then(([result1, result2]) => {
    console.log(result1, result2);
  })
  .catch(error => {
    console.log(error);
  });
