// A Promise is resolved/rejected for EVERYONE

const promise = new Promise(resolve => {
  resolve("resolved");
});

// Evel Process
promise
  .then(() => {
    return Promise.reject("rejected").catch(result => {
      console.log("Evil-then catch", result);
    });
  })
  .then(result => {
    console.log("Evil-then-then", result);
  })
  .catch(result => {
    console.log("Evil-then-then-catch", result);
  });

promise.then(result => {
  console.log("then", result);
});
