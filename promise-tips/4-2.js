// Promise Constructor is not the solution
const fs = require("fs");

const fetchData = () => {
  return new Promise((resolve, reject) => {
    fs.readFile("./static/4.json", (error, data) => {
      if (error) {
        reject(error);
        return;
      }

      resolve(data);
    });
  });
};

new Promise((resolve, reject) => {
  const filePromise = fetchData();
  filePromise
    .then(data => {
      resolve(data);
    })
    .catch(error => {
      reject(error);
    });
})
  .then(result => {
    console.log("then", result);
  })
  .catch(result => {
    console.log("catch", result);
  });
