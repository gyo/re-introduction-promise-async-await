// Avoid the .then hell

const request = () => {
  return Promise.reject({
    statusCode: 400
  });
};

request()
  .then(result => {
    console.log("then", result);
  })
  .catch(result => {
    console.log("catch", result);
    if (result.statusCode === 400) {
      return request();
    }

    return Promise.reject(result);
  })
  .then(result => {
    console.log("then2", result);
  })
  .catch(result => {
    console.log("caatch-catch", result);
  });
