// You can return a Promise inside a .then

Promise.resolve("resolved")
  .then(result1 => {
    return result1;
  })
  .then(result2 => {
    return new Promise((resolve, reject) => {
      reject(result2);
    });
  })
  .then(result3 => {
    console.log(result3);
  })
  .catch(catched => {
    console.log("catched", catched);
  });
