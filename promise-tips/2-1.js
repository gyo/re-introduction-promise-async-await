// You create a new Promise everytime you do .then

const promise1 = new Promise(resolve => {
  setTimeout(() => {
    resolve({ statusCode: 200 });
  }, 1000);
});

const promise2 = promise1.then(response => {
  console.log("promise2");
  return response.statusCode === 200 ? "good" : "bad";
});

const promise3 = promise2.then(result => {
  console.log("promise3");
  return result === "good" ? "ALL OK" : "NOT OK";
});

promise3
  .then(result => console.log("promise3-then", result))
  .catch(catched => console.log("catched", catched));

const promise4 = promise1.then(result => {
  console.log("promise4");
  return Promise.resolve(result);
});

promise4
  .then(result => console.log("promise4-then", result))
  .catch(catched => console.log("catched", catched));

console.log("FINISHED");
