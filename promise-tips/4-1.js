// Promise Constructor is not the solution
const fs = require("fs");

const fetchData = () => {
  return new Promise((resolve, reject) => {
    fs.readFile("./static/4.json", (error, data) => {
      if (error) {
        reject(error);
        return;
      }

      resolve(data);
    });
  });
};

fetchData()
  .then(result => {
    console.log("then", result);
  })
  .catch(result => {
    console.log("catch", result);
  });
