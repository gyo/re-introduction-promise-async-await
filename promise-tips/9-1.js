// Avoid the .then hell

const request = () => {
  return Promise.reject({
    statusCode: 400
  });
};

request()
  .then(result => {
    console.log("then", result);
  })
  .catch(result => {
    console.log("catch", result);
    if (result.statusCode === 400) {
      return request()
        .then(result => {
          console.log("catch-then", result);
        })
        .catch(result => {
          console.log("caatch-catch", result);
        });
    }
  });
