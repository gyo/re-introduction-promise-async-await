const getUsers = () => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve("users");
    }, 1000);
  });
};

const getEvents = async () => {
  return "events";
};

(async () => {
  const result = await Promise.all([getUsers(), getEvents()]);
  console.log(result);
})();
