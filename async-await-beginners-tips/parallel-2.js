const getUsers = () => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve("users");
    }, 1000);
  });
};

const getEvents = async () => {
  return "events";
};

Promise.all([getUsers(), getEvents()]).then(result => {
  console.log(result);
});
