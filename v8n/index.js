const v8n = require("v8n");

const myAsyncRule = expected => {
  return value => {
    return new Promise((resolve, reject) => {
      if (value === expected) {
        resolve(value);
      } else {
        reject(value);
      }
    });
  };
};
v8n.extend({ myAsyncRule });

const aV8n = v8n().myAsyncRule("a");
const bV8n = v8n().myAsyncRule("b");
const cV8n = v8n().myAsyncRule("c");
const dV8n = v8n().myAsyncRule("d");

(async () => {
  const aResult = await aV8n.testAsync("a");
  const bResult = await bV8n.testAsync("b");
  const cResult = await cV8n.testAsync("c");
  const dResult = await dV8n.testAsync("d");

  console.log(aResult, bResult, cResult, dResult);
})();

(async () => {
  const aResult = aV8n.testAsync("a");
  const bResult = bV8n.testAsync("b");
  const cResult = cV8n.testAsync("c");
  const dResult = dV8n.testAsync("d");

  await aResult;
  await bResult;
  await cResult;
  await dResult;
  console.log(aResult, bResult, cResult, dResult);
})();
