(async () => {
  console.log("start");
  const value = await new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(0);
      // reject(0);
    }, 1000);
  })
    .then(value => {
      return "then";
    })
    .catch(error => {
      return "catch";
    });

  console.log(value);
})();
