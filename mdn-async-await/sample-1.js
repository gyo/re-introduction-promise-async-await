(async () => {
  console.log("start");
  const value = await new Promise(resolve => {
    setTimeout(() => {
      resolve(0);
    }, 1000);
  });

  console.log(value);
})();
