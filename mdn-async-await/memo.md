# async await 再入門

## await

```javascript
async () => {
  const value = await new Promise();
};
```

`Promise` が `await` 式で停止された場合、resolve された値が返るまで待つ。

`Promise` が reject された場合、その値を throw する。

`await` 式に続く値が `Promise` ではなかった場合、resolved Promise に変換される。

## async

`AsyncFunction` オブジェクトを返す非同期関数を定義する。

`return` は resolved Promise になる。

`throw` は rejected Promise になる。
