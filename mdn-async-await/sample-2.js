(async () => {
  console.log("start");
  try {
    const value = await new Promise((_, reject) => {
      setTimeout(() => {
        reject(0);
      }, 1000);
    });
    console.log(value);
  } catch (e) {
    console.log("catched");
  }
})();
